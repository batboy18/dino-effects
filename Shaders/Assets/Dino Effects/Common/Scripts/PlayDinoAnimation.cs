﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayDinoAnimation : MonoBehaviour
{
    #region PUBLIC_VARIABLES
    public Animator animator;
    #endregion

    #region PRIVATE_VARIABLES
    #endregion

    #region UNITY_CALLBACKS
    public void Start()
    {
        if(GetComponent<Animator>() != null)
        {
            animator = GetComponent<Animator>();
        }
    }
    #endregion

    #region PUBLIC_METHODS
    [ContextMenu("Play")]
    public void Play()
    {
        animator.Play("Anim");
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        animator.Play("None");
    }
    #endregion

    #region PRIVATE_METHODS
    #endregion

    #region EVENT_HANDLERS
    #endregion

    #region CO-ROUTINES
    #endregion
}
